import modelo.Persona;

public class PersonaApp {

	public static void main(String[] args) {
		Persona persona1 = new Persona();
		System.out.println(persona1);
		
		Persona persona2 = new Persona("Alberto",14,'H');
		System.out.println(persona2);
		
		Persona persona3 = new Persona("Alberto",17,"0000434",'H',14.3,16.3);
		System.out.println(persona3);

	}

}
