package modelo;

public class Persona {
	
	private String nombre;
	private int edad;
	private String dni;
	final private char SEXO;	// Creamos los atributos
	private double peso;
	private double altura;
	
	
	public Persona() {

		this.nombre =" ";
		this.edad = 0;	// Asignamos el valor default

		this.dni = "300000A";
		this.SEXO = 'H';
		this.peso = 0.0;
		this.altura = 0.0;
	}


	public Persona(String nombre, int edad, char sEXO) {
		super();
		this.nombre = nombre;
		this.edad = edad;
		this.dni="00000000";	 // Generamos el constructor
		SEXO = sEXO;
		this.peso = 0;
		this.altura = 0;
		
	}


	public Persona(String nombre, int edad, String dni, char sEXO, double peso, double altura) {
		super();
		this.nombre = nombre;
		this.edad = edad;
		this.dni = dni;
		SEXO = sEXO;	 // Generamos el constructor
		this.peso = peso;
		this.altura = altura;
	}


	@Override
	public String toString() {
		return "Persona [nombre=" + nombre + ", edad=" + edad + ", dni=" + dni + ", SEXO=" + SEXO + ", peso=" + peso
				+ ", altura=" + altura + "]";// Hacemos el toString
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
